import pytest
from helpers import Helper
from reporter_helpers import load_file, assert_signal_is_rising, report_test_case

@pytest.fixture
def helper():
    ''' Return a helper object that can manipulate the DUT '''
    return Helper()

def report_feature_a(meas_file):
    ''' For demo purposes we are going to load a measurement file and then assert that the signal_a is 5'''
    data = load_file(meas_file)
    assert_signal_is_rising(data['A'][0]) # We make assertions here

@report_test_case(report_feature_a) # Decorate the test function and specify the reporter we want to execute afterwards
def test_feature_a(helper:Helper):
    helper.step1()
    helper.step2()
    helper.step3()

def report_feature_b(meas_file):
    ''' For demo purposes we are going to load a measurement file and then assert that the signal_a is 5'''
    data = load_file(meas_file)
    assert_signal_is_rising(data['B'][0]) # We make assertions here

@report_test_case(report_feature_b) # Decorate the test function and specify the reporter we want to execute afterwards
def test_feature_b(helper:Helper):
    helper.step1()
    helper.step2()
    helper.step3()