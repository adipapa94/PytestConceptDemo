from context import test_context

def pytest_addoption(parser):
    parser.addoption('--report-only', action = 'store')

def pytest_configure(config):
    config.report_only = config.getoption('--report-only')

def pytest_collection_modifyitems(config, items):
    if config.report_only:
        test_context.ReportOnly = True
        test_context.MeasFile = config.report_only
    else:
        test_context.MeasFile = 'meas_file.mat'