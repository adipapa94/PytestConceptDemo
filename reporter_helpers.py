from scipy.io import loadmat
from functools import wraps
from context import test_context

def report_test_case(reporter):
    def decorator(test):
        @wraps(test)
        def execute_test(*args, **kwargs):
            if not test_context.ReportOnly:
                test(*args, **kwargs)
            reporter(test_context.MeasFile)
        return execute_test
    return decorator

def load_file(file:str):
    print(f'Loading measurement file: {file}')
    return loadmat(file)

def assert_signal_is_rising(signal):
    for i in range(1, len(signal)):
        if signal[i] <= signal[i - 1]:
            assert False
    assert True

def assert_signal_is_falling(signal):
    for i in range(1, len(signal)):
        if signal[i] >= signal[i - 1]:
            assert False
    assert True