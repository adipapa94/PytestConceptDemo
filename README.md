# Pytest report only demo
In this demo project you can see how you can have separated test executor and reporter functions with Pytest.

This solution showcases how flexible Pytest and Python can be.

# Usage
Execute the runner.py only with the --report-only flag and the path of the meas_file.mat to report already defined and run test cases again without the need to retest. You can change the report_ functions however you like to try how this process can work.

Define test cases and corresponding runners, execute them and try reusing the already available measurement files.