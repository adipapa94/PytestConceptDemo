class TestContext:

    def __init__(self) -> None:
        self._report_only = False
        self._meas_file = ''

    @property
    def ReportOnly(self)->bool:
        return self._report_only
    
    @ReportOnly.setter
    def ReportOnly(self, value:bool):
        self._report_only = value

    @property
    def MeasFile(self)->str:
        return self._meas_file
    
    @MeasFile.setter
    def MeasFile(self, value:str):
        self._meas_file = value

test_context = TestContext()