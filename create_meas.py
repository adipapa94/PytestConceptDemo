import numpy as np
from scipy.io import savemat, loadmat

# A = np.array([1,2,3,4,5,6])
# B = np.array([6,5,4,3,2,1])

# data = {
#     'A':A,
#     'B':B
# }

# savemat('meas_file.mat', data)

data = loadmat('meas_file.mat')

print(f'{data}\n\n\n')

A = data['A'][0]
B = data['B'][0]

print(A)
print(B)